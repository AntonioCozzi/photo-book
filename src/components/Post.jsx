import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";

import Card from './Card'
import PostsList from './PostsList';

export default function Post() {
    const { id } = useParams()
    const [post, setPost] = useState({})

    useEffect(() => {
        fetch(`https://jsonplaceholder.typicode.com/photos/${id}`)
            .then(response => response.json())
            .then(post => setPost(post))
            .catch(error => console.log("Error from Post.jsx:", error))
    }, [])

    return (
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="row">
                        <div className="col-8">
                            <Card post={post}/>
                        </div>
                        <div className="col-4">
                            <PostsList numberOfCols={12}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
