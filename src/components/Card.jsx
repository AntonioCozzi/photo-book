import React from 'react'

export default function Card(props) {
    const { post, isSmall } = props

    return (
        isSmall ?
            <>
                <img src={post.thumbnailUrl} />
                <div className="row">
                    <p className="col-6">{post.title.length >= 15 ? post.title.slice(0, 15) + '...' : post.title}</p>
                    <p className="col-6">{post.id}</p>
                </div>
            </>
            :
            <>
                <div className="row">
                    <img src={post.url} />
                </div>
                <div className="row d-flex justify-content-between">
                    <p>{post.title}</p>
                    <p>{post.id}</p>
                </div>
            </>
    )
}
