import React from 'react';
import { Switch, BrowserRouter as Router, Route } from 'react-router-dom'
import Post from "./components/Post";
import PostsList from "./components/PostsList";

export default function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact>
          <PostsList numberOfCols={4}/>
        </Route>
        <Route path="/post/:id">
          <Post />
        </Route>
        <Route component={() => <div><p>Not found.</p></div>} />
      </Switch>
    </Router>
  )
}