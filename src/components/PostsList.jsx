import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";

import Card from './Card'

export default function PostsList(props) {
    const { numberOfCols } = props
    const [postsList, setPostsList] = useState([])

    useEffect(() => {
        fetch("https://jsonplaceholder.typicode.com/photos")
            .then(response => response.json())
            .then(json => setPostsList(json))
            .catch(error => console.log(error))
    }, [])

    return (
        <div className="container">
            <ul className="row">
                {
                    postsList.map((post, index) =>
                        <li className={`col-${numberOfCols}`} key={index}>
                            <Link to={`/post/${post.id}`}>
                                {
                                    //TODO: pass numberOfCols in Card props
                                }
                                <Card isSmall post={post}/>
                            </Link>
                        </li>
                    )
                }
            </ul>
        </div>
    )
}
